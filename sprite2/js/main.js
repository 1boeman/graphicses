var width = screen.width;
var height = screen.height;

var canvas = document.getElementById("canvas")
canvas.width = width;
canvas.height = height;
var ctx = canvas.getContext("2d")
ctx.fillStyle = "red"

var state = {
  pressedKeys: {
    left: false,
    right: false,
    up: false,
    down: false
  }
}

var keyMap = {
  68: 'right',
  65: 'left',
  87: 'up',
  83: 'down'
}
function keydown(event) {
  var key = keyMap[event.keyCode]
  state.pressedKeys[key] = true
}
function keyup(event) {
  var key = keyMap[event.keyCode]
  state.pressedKeys[key] = false
}



class Sprite{

  constructor(x,y,imagesWalking,imageStopped,options){
    this.x = x;
    this.y = y; 
    this.rotation=0;
    this.imagesWalking = imagesWalking || false;
    this.imagesStopped = imagesStopped || false;
    this.loadedImagesWalking = []; 
    this.loadedImagesStopped = [];
    this.totalImageLength = this.imagesWalking.length + this.imagesStopped.length; 
    this.imgLoaded = false; 
    this.loadImages(this.imagesWalking,this.loadedImagesWalking);
    this.loadImages(this.imagesStopped,this.loadedImagesStopped);
    this.orienation = 'right';
    this.currentImage = 0; 
    this.currentImageArray = this.loadedImagesStopped;
    this.switchImgTime=130;
    this.timer=Date.now();
    this.walking=false; 
   }

  checkImagesLoaded(){
    if (this.imgLoadedLength == this.totalImageLength){
      this.imgLoaded = true;
    }
  }

  loadImages( imgArr, loadedArr ){
    var that = this; 
    this.imgLoadedLength = 0;
    if (imgArr && imgArr.length) {
      for (let i=0; i < imgArr.length; i++){
        let j = new Image(); 
        j.onload = function(){
          that.imgLoadedLength++;
        }
        j.src = imgArr[i];
        loadedArr.push (j);
      }        
    } else {
      alert('no images!')
    }
  }

  drawImage(image, x, y, scale, rotation){
    if (this.orientation == 'right'){
      ctx.setTransform(scale, 0, 0, scale, x, y); // sets scale and origin
    } else {
      ctx.setTransform(-scale, 0, 0, scale, x, y); // sets scale and origin
    }
    ctx.clearRect(0-x, 0-y, width, height);
    ctx.rotate(rotation);
    ctx.drawImage(image, -image.width / 2, -image.height / 2);
  } 

  draw() {
    if (this.imgLoaded || this.checkImagesLoaded()){
      this.drawImage(this.currentImageArray[this.currentImage],this.x,this.y,1,this.rotation);
    }
  }

  upCurrentImage(){
      this.currentImage++; 
      if (this.currentImage >= this.currentImageArray.length){
        this.currentImage=0; 
      }
  }

  setOrientation(orientation){
    this.orientation = orientation;  
  }

  walk(orientation){
    this.setOrientation(orientation);
    this.currentImage = 0; 
    this.walking = true;
    this.currentImageArray = this.loadedImagesWalking;
  }
  stop(){
    this.currentImage = 0; 
    this.walking = false;
    this.currentImageArray = this.loadedImagesStopped;
  }

  update(progress) {
    let timestamp = Date.now();
    if (timestamp - this.timer >= this.switchImgTime){
      this.timer = timestamp; 
      this.upCurrentImage();
    }
    
    
    if (state.pressedKeys.left) {
      if (!this.walking) this.walk('left');
      //this.x -= progress
      //this.rotation = -90 * Math.PI / 180;
    }else if (state.pressedKeys.right) {

      if (!this.walking) this.walk('right');
      //this.x += progress
      //this.rotation = 90 * Math.PI / 180;
    } else{
      this.stop()
    }
    /*
    if (state.pressedKeys.up) {
      this.y -= progress
      this.rotation = 0;
    }
    if (state.pressedKeys.down) {
      this.y += progress
      this.rotation = 180 * Math.PI / 180;
    }

    // Flip position at boundaries
    if (this.x > width) {
      this.x -= width
    }
    else if (this.x < 0) {
      this.x += width
    }
    if (this.y > height) {
      this.y -= height
    }
    else if (this.y < 0) {
      this.y += height
    }
    */
  }
}


function loop(timestamp) {
  var progress = timestamp - lastRender

  ship.update(progress)
  ship.draw()
  
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}

var imagesMoving = [
 'https://jorisosterhaus.nl/graphics/sprite2/img/1.png',

 'https://jorisosterhaus.nl/graphics/sprite2/img/2.png',

 'https://jorisosterhaus.nl/graphics/sprite2/img/4.png',
 'https://jorisosterhaus.nl/graphics/sprite2/img/3.png',



 'https://jorisosterhaus.nl/graphics/sprite2/img/5.png',
 'https://jorisosterhaus.nl/graphics/sprite2/img/6.png',
 'https://jorisosterhaus.nl/graphics/sprite2/img/7.png',
];

var imagesStopped = [
 'https://jorisosterhaus.nl/graphics/sprite2/img/10_standing.png'
]
var options = {};
var ship = new Sprite(width/2,height/2,imagesMoving, imagesStopped);
var lastRender = 0
window.requestAnimationFrame(loop)


window.addEventListener("keydown", keydown, false)
window.addEventListener("keyup", keyup, false)
