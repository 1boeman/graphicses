var canvas = document.getElementById("canvas");
canvas.width = screen.width // Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
canvas.height = screen.height;// Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var width = canvas.width;
var height = canvas.height;
var ctx = canvas.getContext("2d");
ctx.fillStyle = "lime";
var lastRender = 0;

class Grid{
  
  constructor(options){
    this.shapes = ['circle','block','triangle','erase','strokeRect','strokeCircle','ellipse','ellipse'];
    this.gridWidth = options.gridWidth || 100;
    this.width = options.width;
    this.height = options.height;
    this.iteration = 0; 
    this.center()
    this.rotationDirection = 1;
    this.rotationAngle= 46;
    this.setLineWidth(15);
    this.chooseRandomColor();
    this.chooseRandomStrokeColor();
    this.chooseRandomShape();
    this.blockWidthFactor=1;
    this.blockHeightFactor=1;
    this.implement();
    this.chooseRandomCell();
  }
  getRotationAngle(){
    console.log(this.rotationAngle);
  } 
  implement(){
    this.virtualGrid = []; 
    var cols  = Math.floor(this.width/this.gridWidth);
    var rows = Math.floor(this.height/this.gridWidth);
    for (var i=0; i <= cols; i++){
      for (var j=0; j<=rows; j++){
        let coord = {'x':(i*this.gridWidth),'y':(j*this.gridWidth)};
        this.virtualGrid.push(coord);
      }
    }
  }
  center(){
    var cols  = Math.floor(this.width/this.gridWidth);
    var rows = Math.floor(this.height/this.gridWidth);
    this.activeCell = {'x':(cols/2)*this.gridWidth, 'y': Math.floor((rows/2)*this.gridWidth) }
  }

  moveRight(){
    let newX =(this.activeCell.x+2)
    if (newX >= this.width){
      this.activeCell = {x:0,y:0}
    }
    this.activeCell = {'x':newX,'y':this.activeCell.y};
  }

  randomColor(){
    var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
    return color;
  
  }
  chooseRandomColor(){
		ctx.fillStyle = this.randomColor(); 
  }

  chooseRandomStrokeColor(){
		ctx.strokeStyle = this.randomColor(); 
  }
  switchRotationDirection(){
    if (this.rotationDirection == 1){
      this.rotationDirection = -1;
    }else{
      this.rotationDirection = 1;
    }
  }

  setBlockDimensions(bwf,bhf){
    this.blockWidthFactor=bwf;
    this.blockHeightFactor=bhf;
  }

  setRotationAngle (angle){
    this.rotationAngle=angle;
  }

  rotate(){
   ctx.translate(this.width/2, this.height/2);
	 ctx.rotate(this.rotationDirection * this.rotationAngle * Math.PI / 180);	
   ctx.translate(-this.width/2,-this.height/2);
  } 

  chooseRandomCell(){
    this.activeCell = this.virtualGrid[Math.floor(Math.random() * this.virtualGrid.length)]
  } 

  chooseRandomShape(){
    this.shape = this.shapes[Math.floor(Math.random() * this.shapes.length)];
  }

  getIteration(){
    return this.iteration;
  }

  setIteration(n){
    this.iteration = n;
  }
  
  setLineWidth(width){
    ctx.lineWidth = width;
  }

  setGridWidth(n){
    this.gridWidth = n;
  }
 
  draw(){
    let c = this.activeCell
    if(this.shape == 'block'){
      ctx.fillRect(c.x,c.y,(this.blockWidthFactor * this.gridWidth),(this.blockHeightFactor * this.gridWidth));
    } else if (this.shape=='circle'|| this.shape=='strokeCircle'){
      ctx.beginPath();
      let r = this.gridWidth;
      ctx.arc(c.x-r, c.y-r,r, 0, 2 * Math.PI, false);
      if (this.shape =='circle'){
        ctx.fill(); 
      } else {
        ctx.stroke();
      }
    } else if(this.shape=='erase'){
      ctx.clearRect(c.x,c.y,this.gridWidth,this.gridWidth);
    } else if(this.shape=='strokeRect'){
      ctx.strokeRect(c.x,c.y,(this.blockWidthFactor * this.gridWidth),(this.blockHeightFactor * this.gridWidth));
    } else if (this.shape=='ellipse'){
      ctx.beginPath();
      ctx.ellipse(c.x,c.y,(this.blockWidthFactor*this.gridWidth),(this.blockHeightFactor*this.gridWidth), Math.PI / 4, 0, 2 * Math.PI);
      ctx.stroke();
    } else if (this.shape =='triangle'){
      ctx.beginPath();
      let theight = this.blockHeightFactor*this.gridWidth; 
      let twidth = this.blockWidthFactor*this.gridWidth;
      ctx.moveTo(c.x, c.y);
      ctx.lineTo(c.x, theight);
      ctx.lineTo(twidth, theight);
      ctx.closePath();
      ctx.stroke();
    }
    this.iteration +=1;
  }
}

function update(progress){
  if (grid.getIteration() > 300){
	  grid.chooseRandomColor();
	  grid.chooseRandomStrokeColor();
    grid.setIteration(0);
    grid.switchRotationDirection();
    grid.setRotationAngle(getRandom(1,355));
    grid.chooseRandomShape();
    grid.chooseRandomCell();
    grid.setGridWidth(getRandom(2,300));
    grid.setLineWidth(getRandom(1,10));
    grid.setBlockDimensions(getRandom(1,6),getRandom(1,10));
  }
  //grid.moveRight();

	grid.draw();
  
  grid.rotate();
}
function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

function loop(timestamp) {
  var progress = timestamp - lastRender
  update(progress)
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}

var options = {"width":width,"height":height,"gridWidth":50}
var grid = new Grid(options); 


window.requestAnimationFrame(loop)

