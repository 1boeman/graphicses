var canvas = document.getElementById("canvas");
canvas.width = screen.width // Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
canvas.height = screen.height;// Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var width = canvas.width;
var height = canvas.height;
var ctx = canvas.getContext("2d");
ctx.fillStyle = "lime";
ctx.strokeStyle = "#FF0000";
ctx.lineWidth = 1; 
var lastRender = 0;

class Line{
  
  constructor(options){
    this.x = options.x
    this.y = options.y
    this.directionX = 'right';
    this.directionY = 'down';
    ctx.beginPath();
    ctx.moveTo(this.x,this.y)
  }
  
  chooseDirection(){
    let moveX = getRandom(1,20);
    let moveY = getRandom(1,20);
    if (this.x < 0){
      this.directionX = 'right';  
    } else if (this.x > width){
      this.directionX  = 'left';
    }

    if (this.y < 0){
      this.directionY = 'down';
    }else if (this.y > height){
      this.directionY = 'up';
    }

    if (this.directionX == 'right'){
      this.x = this.x+moveX;
    } else { 
      this.x = this.x-moveX;
    }

    if (this.directionY == 'down'){
      this.y = this.y+moveY;
    }else{
      this.y = this.y-moveY;
    }
    console.log(this.x);
    console.log(this.y);
  }

  draw(){
    this.chooseDirection();
    ctx.lineTo(this.x,this.y);
    ctx.stroke(); 
  }
}

function update(progress){
  //ctx.lineWidth = (getRandom(1,2));
  line.draw();
}


function loop(timestamp) {
  var progress = timestamp - lastRender
  update(progress)
  lastRender = timestamp
  
  window.requestAnimationFrame(loop)
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

function randomColor(){
    var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
    return color;
}

function  chooseRandomColor(){
		ctx.strokeStyle = this.randomColor(); 
}



var options = {"x":10,"y":10}
var line = new Line(options); 


window.requestAnimationFrame(loop)

