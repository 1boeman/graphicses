var canvas = document.getElementById("canvas");
canvas.width = screen.width // Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
canvas.height = screen.height;// Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var width = canvas.width;
var height = canvas.height;
var ctx = canvas.getContext("2d");
var lastRender = 0;

class Spiral{
  
  constructor(options){
    this.active=1;
    this.rotationDirection = 1;
    this.iterations = 0; 
    this.maxIterations = options.maxIterations || 1000;
    this.rotationAngle= 1;
    this.setLineWidth(15);
    this.x = options.x;
    this.y = options.y;
    this.r = options.r;
    this.width = canvas.width;
    this.height = canvas.height;
    this.color = options.color;
  }
  
  switchRotationDirection(){
    if (this.rotationDirection == 1){
      this.rotationDirection = -1;
    }else{
      this.rotationDirection = 1;
    }
  }

  setRotationAngle (angle){
    this.rotationAngle=angle;
  }

  setLineWidth(width){
    ctx.lineWidth = width;
  }


  rotate(){
   if (!this.active) return; 
   ctx.translate(this.width/2, this.height/2);
	 ctx.rotate(this.rotationDirection * this.rotationAngle * Math.PI / 180);	
   ctx.translate(-this.width/2,-this.height/2);
  } 
 
  draw(){
    if (!this.active) return; 
    ctx.fillStyle = this.color;
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
    ctx.fill(); 
    this.x+=0.2;
    this.y+=0.2;
    this.iterations +=1; 
    if (this.iterations == this.maxIterations){
      this.active = 0; 
    }
  }
}

function update(progress){
  for (let x in sList){
    sList[x].draw();
    sList[x].rotate();
    if (!sList[x].active){
      delete sList[x];
      sList.push(new Spiral(optionList[0]))
    }
  }
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

function loop(timestamp) {
  var progress = timestamp - lastRender
  update(progress)
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}

var sList=[];
var optionList = [
 {"x":canvas.width/2,"y":canvas.height/2,"r":10,"color":"lime"},
 {"x":canvas.width/3,"y":canvas.height/3,"r":30, "color":"green"},
 {"x":canvas.width/5,"y":canvas.height/5,"r":20, "color":"yellow"},
];
for (let x in optionList){
  sList.push(new Spiral(optionList[x]))
}


window.requestAnimationFrame(loop)

