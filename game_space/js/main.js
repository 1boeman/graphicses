var width = screen.width;
var height = screen.height;

var canvas = document.getElementById("canvas")
canvas.width = width;
canvas.height = height;
var ctx = canvas.getContext("2d")
ctx.fillStyle = "red"

var state = {
  pressedKeys: {
    left: false,
    right: false,
    up: false,
    down: false
  }
}

var keyMap = {
  68: 'right',
  65: 'left',
  87: 'up',
  83: 'down'
}
function keydown(event) {
  var key = keyMap[event.keyCode]
  state.pressedKeys[key] = true
}
function keyup(event) {
  var key = keyMap[event.keyCode]
  state.pressedKeys[key] = false
}



class Ship{

  constructor(x,y){
    this.x = x;
    this.rotation=0;
    this.y = y; 
    this.img1_loaded = false; 
    this.img1 = new Image();
    var that = this;
    this.img1.onload = function(){ 
      that.img1_loaded=true;
    }
    this.img1.src = 'https://jorisosterhaus.nl/graphics/game_space/img/ship.png';
  }

  drawImage(image, x, y, scale, rotation){
    ctx.setTransform(scale, 0, 0, scale, x, y); // sets scale and origin
    ctx.clearRect(0-x, 0-y, width, height);
    ctx.rotate(rotation);
    ctx.drawImage(image, -image.width / 2, -image.height / 2);
  } 

  draw() {
    if (this.img1_loaded){
      console.log(width);
      console.log(height);
      this.drawImage(this.img1,this.x,this.y,1,this.rotation);
      //ctx.drawImage(this.img1, this.x, this.y);
    }
  }


  update(progress) {
    if (state.pressedKeys.left) {
      this.x -= progress
      this.rotation = -90 * Math.PI / 180;
    }
    if (state.pressedKeys.right) {
      this.x += progress
      this.rotation = 90 * Math.PI / 180;
    }
    if (state.pressedKeys.up) {
      this.y -= progress
      this.rotation = 0;
    }
    if (state.pressedKeys.down) {
      this.y += progress
      this.rotation = 180 * Math.PI / 180;
    }

    // Flip position at boundaries
    if (this.x > width) {
      this.x -= width
    }
    else if (this.x < 0) {
      this.x += width
    }
    if (this.y > height) {
      this.y -= height
    }
    else if (this.y < 0) {
      this.y += height
    }
  }
}


function loop(timestamp) {
  var progress = timestamp - lastRender

  ship.update(progress)
  ship.draw()
  
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}

var ship = new Ship(width/2,height/2);
var lastRender = 0
window.requestAnimationFrame(loop)




window.addEventListener("keydown", keydown, false)
window.addEventListener("keyup", keyup, false)
