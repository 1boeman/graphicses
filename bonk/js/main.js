var canvas = document.getElementById("canvas")
canvas.width = screen.width;
canvas.height = screen.height;
var width = canvas.width
var height = canvas.height
var ctx = canvas.getContext("2d")
ctx.fillStyle = "red"
var lastRender = 0;

var bonkContainer = [];
 
class Bonk{
  constructor(options){
    this.image = options.image || false; 
    this.imageLoaded = 0; 
    this.img; 
    this.height = options.height || 100;
    this.width = options.width || 100;
    this.x = options.x || 0;
    this.y = options.y || 0;
    this.speed = options.speed || 0.25;
    this.directionx='right';
    this.directiony='up';
    this.halfWidth=this.width/2;
    this.halfHeight=this.height/2;
    this.inCollision=0;
  }

  draw(){
    if (this.image){
      if (this.imageLoaded == 2){
        ctx.drawImage(this.img,this.x,this.y);
      } else if (this.imageLoaded == 1){
        return false;
      } else {
        var bonk = this;
        this.img = new Image();
        this.imageLoaded = 1;
        this.img.onload = function(){
          bonk.imageLoaded = 2;
        }
        this.img.src = this.image; 
      }
    } else {
      ctx.fillRect(this.x, this.y, this.width, this.height);
    }      
    return true; 
  }
  

  lastCollision(){
    return this.inCollision;
  } 

  takeDirections(bonk){
    this.directionx = bonk.directionx;
    this.directiony = bonk.directiony;
  }

  switchDirection(axis){
    if (axis == 'x'){
      if (this.directionx == 'right'){ 
        this.directionx = 'left';
      } else {
        this.directionx = 'right';
      }
    } else if (axis == 'y'){
      if (this.directiony == 'up'){
        this.directiony = 'down';
      } else {
        this.directiony = 'up';
      }
    }
  }

  update(progress){
    var progress = progress*this.speed
    
    if (this.directionx=='right'){
      this.x += progress
    } else{
      this.x -= progress
    }
    
    if (this.directiony=='up'){
      this.y -= progress
    } else{
      this.y += progress
    }

    // collision
    var collision = false; 
    for (var i =0; i< bonkContainer.length; i++){
      if ( bonkContainer[i] !== this && this.speed < bonkContainer[i].speed){
        //xcollision
        if ((this.x + this.width) > bonkContainer[i].x &&
              this.x < (bonkContainer[i].x+bonkContainer[i].width)){
          //ycollision
          if ((this.y+this.height) > bonkContainer[i].y &&
                this.y < (bonkContainer[i].y+bonkContainer[i].height)){
            //collide 
            let collision = Date.now();
            if (collision - this.inCollision > 200){
              this.inCollision = collision; 
              bonkContainer[i].inCollision = collision; 
              let oldspeed = this.speed;
              this.speed = bonkContainer[i].speed
              bonkContainer[i].speed = oldspeed; 
              this.takeDirections(bonkContainer[i]);
              bonkContainer[i].switchDirection('x');
              bonkContainer[i].switchDirection('y');
            } 
          }
        }
      }
    }

    if (this.x +this.width > width) {
      //state.x -= width
      this.directionx='left';
    }
    else if (this.x < 0) {
      //state.x += width
      this.directionx='right';
    }

    if (this.y +this.height > height) {
     // state.y -= height
      this.directiony='up';
    }
    else if (this.y < 0) {
      //state.y += height
      this.directiony='down';
    }
  }
}

function update(progress){
  for (var i of bonkContainer){
    i.update(progress)
  } 
}

function draw(){
  ctx.clearRect(0, 0, width, height)
  for (var i of bonkContainer){
    i.draw()
  } 
}

function loop(timestamp) {
  var progress = timestamp - lastRender
  update(progress)
  draw()
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}

//https://stackoverflow.com/questions/2677671/how-do-i-rotate-a-single-object-on-an-html-5-canvas
function drawImageRot(img,x,y,width,height,deg){
    // Store the current context state (i.e. rotation, translation etc..)
    ctx.save()

    //Convert degrees to radian 
    var rad = deg * Math.PI / 180;

    //Set the origin to the center of the image
    ctx.translate(x + width / 2, y + height / 2);

    //Rotate the canvas around the origin
    ctx.rotate(rad);

    //draw the image    
    ctx.drawImage(img,width / 2 * (-1),height / 2 * (-1),width,height);

    // Restore canvas state as saved from above
    ctx.restore();
}


var options1 = {
  "image":"https://jorisosterhaus.nl/graphics/bonk/img/demon.png"
}

var options2 = {
  "image":"https://jorisosterhaus.nl/graphics/bonk/img/demon.png",
	"speed":1
}

var options3 = {
  "image":"https://jorisosterhaus.nl/graphics/bonk/img/demon.png",
	"speed":0.5
}

var options4 = {
  "image":"https://jorisosterhaus.nl/graphics/bonk/img/joe.png",
	"speed":0.1
}



bonkContainer.push(new Bonk(options1));
bonkContainer.push(new Bonk(options2));
bonkContainer.push(new Bonk(options3));
bonkContainer.push(new Bonk(options4));




window.requestAnimationFrame(loop)

