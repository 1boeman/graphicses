var width = screen.width;
var height = screen.height;

var canvas = document.getElementById("canvas")
canvas.width = width;
canvas.height = height;
var ctx = canvas.getContext("2d")
ctx.fillStyle = "red"

var state = {
  pressedKeys: {
    left: false,
    right: false,
    up: false,
    down: false
  }
}

var keyMap = {
  68: 'right',
  65: 'left',
  87: 'up',
  83: 'down'
}
function keydown(event) {
  var key = keyMap[event.keyCode]
  state.pressedKeys[key] = true
}
function keyup(event) {
  var key = keyMap[event.keyCode]
  state.pressedKeys[key] = false
}



class Sprite{

  constructor(x,y,images,options){
    this.x = x;
    this.y = y; 
    this.rotation=0;
    this.images = images || false;
    this.loadedImages = []; 
    this.imgLoaded = false; 
    this.loadImages();
    this.currentImage = 0; 
    this.switchImgTime=100;
    this.timer=Date.now();
  }

  loadImages(){
    var that = this; 
    this.imgLoadedLength = 0;
    if (this.images && this.images.length) {
      for (let i=0; i < images.length; i++){
        let j = new Image(); 
        j.onload = function(){
          that.imgLoadedLength++;
          console.log(this.src);
          if (that.imgLoadedLength == that.images.length){
            that.imgLoaded = true;
          }
        }
        j.src = this.images[i];
        this.loadedImages.push (j);
      }        
    } else {
      alert('no images!')
    }
  
  }

  drawImage(image, x, y, scale, rotation){
    ctx.setTransform(scale, 0, 0, scale, x, y); // sets scale and origin
    ctx.clearRect(0-x, 0-y, width, height);
    ctx.rotate(rotation);
    ctx.drawImage(image, -image.width / 2, -image.height / 2);
  } 

  draw() {
    console.log(this.imgLoaded)
    if (this.imgLoaded){
      this.drawImage(this.loadedImages[this.currentImage],this.x,this.y,1,this.rotation);
    }
  }

  upCurrentImage(){
    this.currentImage++; 
    this.x+=1;
    if (this.currentImage >7){
      this.y+=5;
    }else{
      this.y-=5;
    }
    if (this.currentImage >= this.images.length){
      this.currentImage=0; 
    }
  }

  update(progress) {
    let timestamp = Date.now();
    if (timestamp - this.timer >= this.switchImgTime){
      this.timer = timestamp; 
      this.upCurrentImage();
    }

    
/*
    if (state.pressedKeys.left) {
      this.x -= progress
      this.rotation = -90 * Math.PI / 180;
    }
    if (state.pressedKeys.right) {
      this.x += progress
      this.rotation = 90 * Math.PI / 180;
    }
    if (state.pressedKeys.up) {
      this.y -= progress
      this.rotation = 0;
    }
    if (state.pressedKeys.down) {
      this.y += progress
      this.rotation = 180 * Math.PI / 180;
    }

    // Flip position at boundaries
    if (this.x > width) {
      this.x -= width
    }
    else if (this.x < 0) {
      this.x += width
    }
    if (this.y > height) {
      this.y -= height
    }
    else if (this.y < 0) {
      this.y += height
    }
    */
  }
}


function loop(timestamp) {
  var progress = timestamp - lastRender

  ship.update(progress)
  ship.draw()
  
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}

var images = [
 'https://jorisosterhaus.nl/graphics/sprite/img/6.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/5.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/4.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/3.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/1.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/2.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/7.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/7.png',


 'https://jorisosterhaus.nl/graphics/sprite/img/2.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/1.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/3.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/4.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/5.png',
 'https://jorisosterhaus.nl/graphics/sprite/img/6.png',
 
];
var options = {};
var ship = new Sprite(width/2,height/2,images);
var lastRender = 0
window.requestAnimationFrame(loop)




window.addEventListener("keydown", keydown, false)
window.addEventListener("keyup", keyup, false)
