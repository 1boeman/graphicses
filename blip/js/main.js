var canvas = document.getElementById("canvas");
canvas.width = screen.width // Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
canvas.height = screen.height;// Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var width = canvas.width;
var height = canvas.height;
var ctx = canvas.getContext("2d");
ctx.fillStyle = "lime";
var lastRender = 0;

class Grid{
  
  constructor(options){
    this.gridWidth = options.gridWidth || 100;
    this.width = options.width;
    this.height = options.height;
  }
   
  implement(){
    this.virtualGrid = []; 
    this.shapes = ['erase','block','erase','erase'];
    var cols  = Math.floor(this.width/this.gridWidth);
    var rows = Math.floor(this.height/this.gridWidth);
    for (var i=0; i <= cols; i++){
      for (var j=0; j<=rows; j++){
        let coord = {'x':(i*this.gridWidth),'y':(j*this.gridWidth)};
        this.virtualGrid.push(coord);
      }
    }
  }

  chooseRandomColor(){
		
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		ctx.fillStyle = color; 
  }
  chooseRandomCell(){
    this.activeCell = this.virtualGrid[Math.floor(Math.random() * this.virtualGrid.length)]
  } 

  chooseRandomShape(){
    this.shape = this.shapes[Math.floor(Math.random() * this.shapes.length)];
  }

  draw(){
    let c = this.activeCell
    if(this.shape == 'block'){
      ctx.fillRect(c.x,c.y,this.gridWidth,this.gridWidth);
    } else if (this.shape=='circle'){
      
      ctx.beginPath();
      let r = this.gridWidth/2;
      ctx.arc(c.x-r, c.y-r,r, 0, 2 * Math.PI, false);
      ctx.fill(); 
    }else if(this.shape=='erase'){
       ctx.clearRect(c.x,c.y,this.gridWidth,this.gridWidth);

    }
  }
}

function update(progress){
	//grid.chooseRandomColor();
  grid.chooseRandomCell();
  grid.chooseRandomShape();
	grid.draw();
}


function loop(timestamp) {
  var progress = timestamp - lastRender
  update(progress)
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}

var options = {"width":width,"height":height,"gridWidth":25}
var grid = new Grid(options); 
grid.implement();


window.requestAnimationFrame(loop)

