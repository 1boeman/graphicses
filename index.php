<?php
$pages = [
  'bonk' => [
    'js'=>'bonk/js/main.js',
    'css'=>'bonk/css/style.css',
    'body'=>'<canvas id="canvas" width="1496" height="488"></canvas>',
    
  ],
  'blip' => [
    'js'=>'blip/js/main.js',
    'css'=>'blip/css/style.css',
    'body'=>'<canvas id="canvas"></canvas>',
  ],

  'blip2' => [
    'js'=>'blip2/js/main.js',
    'css'=>'blip2/css/style.css',
    'body'=>'<canvas id="canvas"></canvas>',
  ],
  'blip3' => [
    'js'=>'blip3/js/main.js',
    'css'=>'blip2/css/style.css',
    'body'=>'<canvas id="canvas"></canvas>',
  ],

  'pattern' =>[
    'js'=>'pattern/js/main.js',
    'css'=>['pattern/css/normalize.css','pattern/css/style.css'],
    'body'=> '<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>',
  ],  
  'circle' =>[
    'js'=>'circle/teken.js',
    'body'=>'<canvas id="decanvas"></canvas>',
    'css'=>'circle/style.css'
  ],
  'gradient' =>[
    'js'=>'gradient/teken.js',
    'body'=>'<canvas id="decanvas"></canvas>',
    'css'=>'gradient/style.css'
  ],




];
$dir = array_rand($pages);
$base = getcwd();
$page = $pages[$dir];
$to_be_cc = ['js','css'];
$vars = [];
foreach ($page as $key => $val){
  if (in_array($key,$to_be_cc)){
    if (is_array($val)){
      foreach ($val as $path){
        $target = $base.'/'.$path;
        $content = file_get_contents($target);
        if (isset($vars[$key])){
          $vars[$key] .= "\n";
          $vars[$key] .= $content;
        } else {
          $vars[$key] = $content;
        }
      }  
    } else {
        $target = $base.'/'.$val;
        $content = file_get_contents($target);
        $vars[$key] = $content;
    }
  } elseif ($key == 'body'){
    $vars[$key] = $val; 
  }

}
?><!doctype html>
<html>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <head>
      <meta charset="utf-8">

<style type="text/css">
<?php echo $vars['css']; ?>
</style>

        <title>Joris Osterhaus</title>
    </head>
    <body>
<?php echo $vars['body'] ?> 
<script>
<?php echo $vars['js'] ?>
</script>

    </body>
</html>

